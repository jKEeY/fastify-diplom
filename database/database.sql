DROP TABLE users;
DROP TABLE roles;
DROP TABLE staff;
DROP TABLE position;
DROP TABLE national;
DROP TABLE education;
DROP TABLE family_status;
DROP TABLE child_status;
DROP TABLE department;
DROP TABLE department_staff;
DROP TABLE employee_table;

create table users (
  user_id serial primary key,
  email varchar(255) not null,
  password varchar(255) not null,
  login varchar(255) not null,
  role_reference integer not null
);

create table roles (
  role_id serial primary key,
  role_name varchar(255) not null
);

create table staff (
  staff_id serial primary key,
  first_name varchar(255) not null,
  second_name varchar(255) not null,
  last_name varchar(255) not null,
  position_reference integer not null,
  date_employment varchar(255) not null,
  date_dismissal varchar(255) not null,
  salary integer not null,
  identity_number integer not null,
  date_birth varchar(255) not null,
  place_birtch varchar(255) not null,
  INN bigint not null,
  national_reference integer not null,
  pasport_data bigint not null,
  education_reference integer not null,
  education_place varchar(255) not null,
  family_status_reference integer not null,
  child_status_reference integer not null,
  count_child integer not null,
  work_status varchar(255) not null,
  img_url varchar(255) not null
);

create table department (
  department_id serial not null,
  department_name varchar(255) not null,
  head_department_reference integer
);

create table department_staff(
  department_staff_id serial primary key,
  department_reference integer not null,
  staff_reference integer not null
);

create table position (
  position_id serial primary key,
  position_name varchar(255) not null
);

create table national (
  national_id serial primary key,
  national_name varchar(255) not null
);

create table education (
  education_id serial primary key,
  education_name varchar(255) not null
);

create table family_status (
  family_status_id serial primary key,
  family_status_name varchar(255) not null
);

create table child_status (
  child_status_id serial primary key,
  child_status_name varchar(255) not null
);

create table employee_table (
  employee_table_id serial primary key,
  department_reference integer not null,
  position_reference integer not null
);

insert into roles (role_name) values ('admin');
insert into roles (role_name) values ('moderator');
insert into roles (role_name) values ('employee');

insert into users (email, password, login, role_reference) values ('admin@admin.ru', '$2a$10$Dfmjs.kA1sprAZpy4W4Zm.3bu7cf1pu6YFiETB95RF9P3k5ddWRxS', 'admin', 1);

insert into national (national_name) values ('Россия');
insert into national (national_name) values ('Украина');
insert into national (national_name) values ('Беларусь');

insert into education (education_name) values ('Высшее');
insert into education (education_name) values ('Среднее'); 
insert into education (education_name) values ('Среднее специальноес');

insert into family_status (family_status_name) values ('Женат');
insert into family_status (family_status_name) values ('Холост');

insert into child_status (child_status_name) values ('Есть');
insert into child_status (child_status_name) values ('Нет');

insert into department (department_name) values ('Администрация');
insert into department (department_name) values ('Департамент информационных технологий');
insert into department (department_name) values ('Департамент маркетинга');

insert into position (position_name) values ('Гениральный директор');
insert into position (position_name) values ('Разработчик');
insert into position (position_name) values ('Руководитель отдела информационных технологий');
insert into position (position_name) values ('Руководитель отдела маркетинга');
insert into position (position_name) values ('Менеджер по медиапланированию');
insert into position (position_name) values ('Менеджер по продукту');

insert into staff (
  first_name,
  second_name,
  last_name,
  position_reference,
  date_employment,
  date_dismissal,
  salary,
  identity_number,
  date_birth,
  place_birtch,
  INN,
  national_reference,
  pasport_data,
  education_reference,
  education_place,
  family_status_reference,
  child_status_reference,
  count_child,
  work_status,
  img_url
) values (
  'Илья',
  'Вегнер',
  'Васильевич',
  1,
  '14.01.2022',
  '',
  150000,
  2313,
  '24.06.2002',
  'Реутов',
  5004204526,
  1,
  46162545455,
  3,
  'ГАПОУ МО ПК Энергия',
  2,
  2,
  0,
  'Нашёл',
  'dsad'
);

insert into department_staff (department_reference, staff_reference) values (1, 1);
insert into employee_table (department_reference, position_reference) values (1, 1), (2, 2), (2, 6), (2, 3), (3, 5), (3, 4);

select * from users as t1
inner join roles as t2 on t2.role_id = t1.role_reference;

select * from staff as t1
inner join position as t2 on t2.position_id = t1.position_reference
inner join national as t3 on t3.national_id = t1.national_reference
inner join education as t4 on t4.education_id = t1.education_reference
inner join family_status as t5 on t5.family_status_id = t1.family_status_reference
inner join child_status as t6 on t6.child_status_id = t1.child_status_reference
inner join department_staff as t7 on t7.staff_reference = t1.staff_id
inner join department as t8 on t8.department_id = t7.department_reference;

select position_id, position_name from (
  select * from employee_table as t10
  where t10.department_reference = 2
) as t1
inner join position as t2 on t2.position_id = t1.position_reference;