import Fastify from "fastify";
import fastifySession from '@fastify/session'
import cookie from '@fastify/cookie';
import fastifyStatic from '@fastify/static';

import path from 'path';

import { dbClient } from './packages/postgresql.js';

import useUserRoutes from './modules/adapters/user/user.route.js';
import usePageRoutes from './modules/adapters/pages/pages.route.js';
import useStaffRoutes from './modules/adapters/staff/staff.route.js';
import usePositionRoute from './modules/adapters/position/position.route.js';

const API_PREFIX = '/api';

export const SECRET = 'diplomdiplomdiplomdiplomdiplomdiplomdiplomdiplomdiplomdiplomdiplom';

const fastify = new Fastify({
  logger: true,
});

fastify.get('/_health', async (request, reply) => {
  return { status: 'ok' }
})

const start = async () => {
  try {
    // plugins
    fastify.register(cookie);
    fastify.register(fastifySession, {
      secret: SECRET,
      saveUninitialized: false,
      cookie: {
        httpOnly: true,
        secure: false,
      }
    });

    fastify.register(fastifyStatic, {
      root: path.join(path.resolve(), 'static'),
      prefix: '/static/'
    })

    // routes
    fastify.register(usePageRoutes);
    fastify.register(useUserRoutes, { prefix: API_PREFIX })
    fastify.register(useStaffRoutes, { prefix: API_PREFIX })
    fastify.register(usePositionRoute, { prefix: API_PREFIX })

    await dbClient.connect();
    await fastify.listen(3000)
  } catch (err) {
    await dbClient.die();
    fastify.log.error(err)
    process.exit(0)
  }
}
start()