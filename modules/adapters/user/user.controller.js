import bcrypt from 'bcryptjs';
import { getAllUsers, createUser, getUserByLogin } from '../../user/user.service.js'
import { CreateUserDTO, SigninUserDTO } from '../../user/user.dto.js'

export async function getUsersHandler(_, response) {
  const users = await getAllUsers();

  return response
    .code(200)
    .send(users);
}

export async function userSignupHandler(request, response) {
  const userDTO = new CreateUserDTO(request.body);
  try {
    const hashed = await bcrypt.hash(userDTO.password, 10);
    const user = await createUser({ ...userDTO, password: hashed });

    return response
      .code(200)
      .send(user);
  } catch (err) {
    console.log(err);
    throw new Error('error sign up');
  }
}

export async function userSigninHandler(request, response) {
  const signinDTO = new SigninUserDTO(request.body);
  
  try {
    const user = await getUserByLogin(signinDTO.login);
    if (!user) {
      return response
      .code(403)
      .send({ message: 'Пользователь не найден' });
    }
    const isValid = await bcrypt.compare(signinDTO.password, user.password);
    if (!isValid) {
      return response
      .code(403)
      .send({ message: 'Пароль или логин неверный' });
    }

    request.session.user = { id: user.id };

    return response
      .code(200)
      .send({ success: true });
  } catch (err) {
    throw new Error('error signin');
  }
}