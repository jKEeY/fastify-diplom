import { getUserOptions, userSignupOptions, userSigninOptions } from './user.options.js';
import { getUsersHandler, userSignupHandler, userSigninHandler } from './user.controller.js';

const GET_USERS_URL = '/v1/userService/users';
const USER_SIGNUP = '/v1/userService/signup';
const USER_SIGNIN = '/v1/userService/signin';

export default async function useUserRoutes(server) {
  server.get(GET_USERS_URL, getUserOptions, getUsersHandler);
  server.post(USER_SIGNUP, userSignupOptions, userSignupHandler);
  server.post(USER_SIGNIN, userSigninOptions, userSigninHandler)
}