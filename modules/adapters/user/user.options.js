const USER_RESPONSE = {
  type: "object",
  properties: {
    id: { type: "number" },
    email: { type: "string" },
    login: { type: "string" },
    role_name: { type: "string"}
  }
}

export const getUserOptions = {
  schema: {
    response: {
      200: {
        type: "array",
        items: USER_RESPONSE
      }
    }
  }
}

export const userSignupOptions = {
  schema: {
    body: {
      type: "object",
      required: ['login', 'password', 'email', 'role_reference'],
      properties: {
        login: { type: "string" },
        password: { type: "string" },
        email: { type: "string" },
        role_reference: { type: "number" },
      }
    },
    response: {
      200: USER_RESPONSE
    }
  }
}

export const userSigninOptions = {
  schema: {
    body: {
      type: "object",
      required: ['login', 'password'],
      properties: {
        login: { type: "string" },
        password: { type: "string" },
      }
    },
    response: {
      200: {
        type: "object",
        properties: {
          success: { type: "boolean" },
        }
      },
      403: {
        type: "object",
        properties: {
          message: { type: "string" }
        }
      }
    }
  }
}