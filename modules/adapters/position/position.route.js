import { postPositionsByDepartmentId } from './position.options.js';
import { getPositionsByDepartmentId } from './position.controller.js';

const POST_POSITION_BY_DEPARTMENT_ID = '/v1/positionService/position/department';

export default async function usePositionRoute(server) {
  server.post(POST_POSITION_BY_DEPARTMENT_ID, postPositionsByDepartmentId, getPositionsByDepartmentId)
}