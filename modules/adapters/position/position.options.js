export const postPositionsByDepartmentId = {
  schema: {
    body: {
      type: "object",
      properties: {
        department_id: { type: "string" }
      }
    },
    response: {
      200: {
        type: "array",
        items: {
          type: "object",
          properties: {
            position: { type: "string" },
            position_id: { type: "number" }
          }
        }
      }
    }
  }
}