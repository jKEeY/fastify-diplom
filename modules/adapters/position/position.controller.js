import { getPositionByDepartmentId } from '../../position/position.service.js';
import { PositionListFindDTO } from '../../position/position.dto.js'

export async function getPositionsByDepartmentId(request, response) {
  const { department_id } = new PositionListFindDTO(request.body);

  const result = await getPositionByDepartmentId(department_id);

  return response
    .code(200)
    .send(result);
}