import { getStaffsHandler, getStaffCreateFormHandler, createStaffHandler } from './staff.controller.js';
import { getUserOptions, getStaffFormDataOptions } from './staff.options.js';

const GET_STAFFS_URL = '/v1/staffService/staffs';
const GET_STAFF_FORM_DATA_URL = '/v1/staffService/form_data';
const POST_CREATE_STAFF_URL = '/v1/staffService/staff/create';

export default async function useStaffRoutes(server) {
  server.get(GET_STAFFS_URL, getUserOptions, getStaffsHandler);
  server.get(GET_STAFF_FORM_DATA_URL, getStaffFormDataOptions, getStaffCreateFormHandler)
  server.post(POST_CREATE_STAFF_URL, createStaffHandler);
}