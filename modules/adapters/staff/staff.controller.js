import { getAllStaff, getStaffCreateForm, createStaff } from '../../staff/staff.service.js';
import { StaffCreateDTO } from '../../staff/staff.dto.js';

export async function getStaffsHandler(request, response) {
  const staffs = await getAllStaff();

  return response
    .code(200)
    .send(staffs);
}

export async function getStaffCreateFormHandler(_, response) {
  const formData = await getStaffCreateForm();

  return response
    .code(200)
    .send(formData);
}

export async function createStaffHandler(request, response) {
  const staffDTO = new StaffCreateDTO(request.body);
  await createStaff(staffDTO);

  return response
    .code(200)
    .send({ success: true })
}