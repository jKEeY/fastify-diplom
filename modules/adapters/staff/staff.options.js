export const getUserOptions = {
  schema: {
    response: {
      200: {
        type: "array",
        items: {
          type: "object",
          properties: {
            id: { type: "number" },
            firstName: { type: "string" },
            secondName: { type: "string" },
            lastName: { type: "string" },
            position: { type: "string" },
            dateEmployment: { type: "string" },
            salary: { type: "number" },
            identityNumber: { type: "number" },
            dateBirth: { type: "string" },
            placeBirtch: { type: "string" },
            INN: { type: "number" },
            national: { type: "string" },
            pasportData: { type: "number" },
            education: { type: "string" },
            educationPlace: { type: "string" },
            familyStatus: { type: "string" },
            childStatus: { type: "string" },
            countChild: { type: "number" },
            workStatus: { type: "string" },
            imgUrl: { type: "string" },
            department: { type: "string" },
          }
        }
      }
    }
  }
}

export const getStaffFormDataOptions  = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          nationals: {
            type: "array",
            items: {
              type: "object",
              properties: {
                national: { type: "string" },
                national_id: { type: "number" },
              }
            }
          },
          educations: {
            type: "array",
            items: {
              type: "object",
              properties: {
                education: { type: "string" },
                education_id: { type: "number" },
              }
            }
          },
          familyStatuses: {
            type: "array",
            items: {
              type: "object",
              properties: {
                family_status: { type: "string" },
                family_status_id: { type: "number" },
              }
            }
          },
          childStatuses: {
            type: "array",
            items: {
              type: "object",
              properties: {
                child_status: { type: "string" },
                child_status_id: { type: "number" },
              }
            }
          },
          departments: {
            type: "array",
            items: {
              type: "object",
              properties: {
                department_id: { type: "number" },
                department: { type: "string" },
              }
            }
          },
        }
      }
    }
  }
}