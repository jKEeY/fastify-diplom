import { defaultPageHandler, loginPageHandler, staffsPageHandler } from './pages.controller.js';

const MAIN_PAGE_URL = '/';
const LOGIN_PAGE_URL = '/login';
const STAFFS_PAGE_URL = '/staffs';

function authCheckRoute(request, response, done) {
  const userSession = request.session.user;
  if (!userSession) {
    return response.redirect('/login');
  }

  done();
}

const preHandlerMiddleware = {
  preHandler: [authCheckRoute]
}

export default async function usePageRoutes(server) {
  server.get(MAIN_PAGE_URL, preHandlerMiddleware,defaultPageHandler);
  server.get(STAFFS_PAGE_URL, preHandlerMiddleware, staffsPageHandler);
  server.get(LOGIN_PAGE_URL, loginPageHandler);
}