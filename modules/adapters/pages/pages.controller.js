export async function defaultPageHandler(_, response) {
  return response.sendFile('/pages/index.html');
}

export async function loginPageHandler(request, response) {
  const userSession = request.session.user;
  if (userSession) {
    return response.redirect('/');
  }
  return response.sendFile('/pages/login.html');
}

export async function staffsPageHandler(_, response) {
  return response.sendFile('/pages/staffs.html');
}