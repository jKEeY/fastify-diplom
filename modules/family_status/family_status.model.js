export class FamilyStatusModel {
  constructor(familyStatusModel) {
    this.family_status_id = familyStatusModel.family_status_id;
    this.family_status = familyStatusModel.family_status_name;
  }
}