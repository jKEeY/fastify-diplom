import { dbClient } from '../../packages/postgresql.js';

export class FamilyStatusStorage {
  static async getAllFamilyStatus() {
    return await dbClient.query(`select * from family_status`);
  }
}