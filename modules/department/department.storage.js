import { dbClient } from '../../packages/postgresql.js';

export class DepartmentStorage {
  static async getAllDepartments() {
    return await dbClient.query(`
      select distinct department_id, department_name from (
        select * from employee_table as t1
        inner join department as t2 on t2.department_id = t1.department_reference
      ) as t3;
    `);
  }
}