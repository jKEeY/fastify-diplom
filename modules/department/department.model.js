export class DepartmentModel {
  constructor(departmentModel) {
    this.department_id = departmentModel.department_id;
    this.department = departmentModel.department_name;
  }
}