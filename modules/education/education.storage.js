import { dbClient } from '../../packages/postgresql.js';

export class EducationStorage {
  static async getAllEducation() {
    return await dbClient.query(`select * from education`);
  }
}