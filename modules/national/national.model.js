export class NationalModel {
  constructor(nationalModel) {
    this.national_id = nationalModel.national_id;
    this.national = nationalModel.national_name;
  }
}