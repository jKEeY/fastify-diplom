import { dbClient } from '../../packages/postgresql.js';

export class NationalStorage {
  static async getAllNational() {
    return await dbClient.query(`select * from national;`);
  }
}