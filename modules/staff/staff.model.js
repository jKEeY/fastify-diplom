import { PositionModel } from '../position/position.model.js';
import { NationalModel } from '../national/national.model.js';
import { EducationModel } from '../education/education.model.js';
import { FamilyStatusModel } from '../family_status/family_status.model.js';
import { ChildStatusModel } from '../child_status/child_status.model.js';
import { DepartmentModel } from '../department/department.model.js';
export class StaffModel {
  constructor(staffModel) {
    this.id = staffModel.staff_id;
    this.firstName = staffModel.first_name;
    this.secondName = staffModel.second_name;
    this.lastName = staffModel.last_name;
    this.position = new PositionModel({ position_name: staffModel.position_name }).position
    this.dateEmployment = staffModel.date_employment;
    this.dateDismissal = staffModel.date_dismissal;
    this.salary = staffModel.salary;
    this.identityNumber = staffModel.identity_number;
    this.dateBirth = staffModel.date_birth;
    this.placeBirtch = staffModel.place_birtch;
    this.INN = staffModel.inn;
    this.national = new NationalModel({ national_name: staffModel.national_name }).national;
    this.pasportData = staffModel.pasport_data;
    this.education = new EducationModel({ education_name: staffModel.education_name }).education;
    this.educationPlace = staffModel.education_place;
    this.familyStatus = new FamilyStatusModel({ family_status_name: staffModel.family_status_name }).family_status;
    this.childStatus = new ChildStatusModel({ child_status_name: staffModel.child_status_name }).child_status;
    this.countChild = staffModel.count_child;
    this.workStatus = staffModel.work_status;
    this.imgUrl = staffModel.img_url;
    this.department = new DepartmentModel({ department_name: staffModel.department_name }).department
  }
}