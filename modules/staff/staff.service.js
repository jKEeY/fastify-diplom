import { StaffStorage } from './staff.storage.js';
import { StaffModel } from './staff.model.js';

import { NationalStorage } from '../national/national.storage.js';
import { NationalModel } from '../national/national.model.js';

import { EducationStorage } from '../education/education.storage.js';
import { EducationModel } from '../education/education.model.js';

import { FamilyStatusStorage } from '../family_status/family_status.storage.js';
import { FamilyStatusModel } from '../family_status/family_status.model.js';

import { ChildStatusStorage } from '../child_status/child_status.storage.js';
import { ChildStatusModel } from '../child_status/child_status.model.js';

import { DepartmentStorage } from '../department/department.storage.js';
import { DepartmentModel } from '../department/department.model.js';

export async function getAllStaff() {
  const result = await StaffStorage.getAllStaff();
  const staffs = result.rows.map((staff) => new StaffModel(staff));

  return staffs;
}

export async function getStaffCreateForm() {
  const nationalResult = await NationalStorage.getAllNational();
  const nationals = nationalResult.rows.map((national) => new NationalModel(national))

  const educationResult = await EducationStorage.getAllEducation();
  const educations = educationResult.rows.map((education) => new EducationModel(education))

  const familyResult = await FamilyStatusStorage.getAllFamilyStatus();
  const familyStatuses = familyResult.rows.map(familyStatus => new FamilyStatusModel(familyStatus))

  const childStatusResult = await ChildStatusStorage.getAllChildStatus();
  const childStatuses = childStatusResult.rows.map(childStatus => new ChildStatusModel(childStatus))

  const departmentsResult = await DepartmentStorage.getAllDepartments();
  const departments = departmentsResult.rows.map(department => new DepartmentModel(department));

  return {
    nationals,
    educations,
    familyStatuses,
    childStatuses,
    departments,
  }
}

export async function createStaff(staffDTO) {
  const result = await StaffStorage.create(staffDTO);
  await StaffStorage.createDepartmentStaff(staffDTO.departmentId, result.rows[0].staff_id)
  
  return true;
}