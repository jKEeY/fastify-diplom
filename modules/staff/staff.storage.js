import { dbClient } from '../../packages/postgresql.js';

export class StaffStorage {
  static async getAllStaff() {
    return dbClient.query(`
      select * from staff as t1
      inner join position as t2 on t2.position_id = t1.position_reference
      inner join national as t3 on t3.national_id = t1.national_reference
      inner join education as t4 on t4.education_id = t1.education_reference
      inner join family_status as t5 on t5.family_status_id = t1.family_status_reference
      inner join child_status as t6 on t6.child_status_id = t1.child_status_reference
      inner join department_staff as t7 on t7.staff_reference = t1.staff_id
      inner join department as t8 on t8.department_id = t7.department_reference;
    `)
  }

  static create(dto) {
    return dbClient.query(`
    insert into staff (
      first_name,
      second_name,
      last_name,
      position_reference,
      date_employment,
      date_dismissal,
      salary,
      identity_number,
      date_birth,
      place_birtch,
      INN,
      national_reference,
      pasport_data,
      education_reference,
      education_place,
      family_status_reference,
      child_status_reference,
      count_child,
      work_status,
      img_url
    ) values (
      $1,
      $2,
      $3,
      $4,
      $5,
      $6,
      $7,
      $8,
      $9,
      $10,
      $11,
      $12,
      $13,
      $14,
      $15,
      $16,
      $17,
      $18,
      $19,
      $20
    )
    returning *;
    `, [
      dto.firstName,
      dto.secondName,
      dto.lastName,
      dto.positionId,
      dto.dateEmployment,
      '',
      dto.salary,
      dto.identityNumber,
      dto.dateBirth,
      dto.placeBirtch,
      dto.INN,
      dto.nationalId,
      dto.pasportData,
      dto.educationId,
      dto.educationPlace,
      dto.familyStatusId,
      dto.childStatusId,
      dto.countChild,
      dto.workStatus,
      ''
    ])
  }
  static createDepartmentStaff(departmentId, staffId) {
    return dbClient.query(`
      insert into department_staff (department_reference, staff_reference) values ($1, $2)
    `, [departmentId, staffId])
  }
}