export class StaffCreateDTO {
  constructor(dto) {
    this.firstName = dto.firstName;
    this.lastName = dto.lastName;
    this.secondName = dto.secondName;
    this.departmentId = dto.departmentId;
    this.positionId = dto.positionId;
    this.salary = dto.salary;
    this.identityNumber = dto.identityNumber;
    this.dateBirth = dto.dateBirth;
    this.placeBirtch = dto.placeBirtch;
    this.INN = dto.INN;
    this.nationalId = dto.nationalId;
    this.educationId = dto.educationId;
    this.educationPlace = dto.educationPlace;
    this.familyStatusId = dto.familyStatusId;
    this.pasportData = dto.pasportData;
    this.childStatusId = dto.childStatusId;
    this.countChild = dto.countChild;
    this.workStatus = dto.workStatus;
    this.dateEmployment = dto.dateEmployment;
  }
}