import { dbClient } from '../../packages/postgresql.js';

export class PositionStorage {
  static async getAllPositions() {
    return await dbClient.query(`select * from positions`);
  }

  static async getPositionByDepartmentId(departmentId) {
    return await dbClient.query(`
      select position_id, position_name from (
        select * from employee_table as t10
        where t10.department_reference = $1
      ) as t1
      inner join position as t2 on t2.position_id = t1.position_reference;
    `, [departmentId]);
  }
}