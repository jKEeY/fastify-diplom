import { PositionStorage } from './position.storage.js';
import { PositionModel } from './position.model.js'

export async function getPositionByDepartmentId(departmentId) {
  const result = await PositionStorage.getPositionByDepartmentId(departmentId);
  const positions = result.rows.map(position => new PositionModel(position));

  return positions;
}