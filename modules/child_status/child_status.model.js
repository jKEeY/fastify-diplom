export class ChildStatusModel {
  constructor(childStatusModel) {
    this.child_status_id = childStatusModel.child_status_id;
    this.child_status = childStatusModel.child_status_name;
  }
}