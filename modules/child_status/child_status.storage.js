import { dbClient } from '../../packages/postgresql.js';

export class ChildStatusStorage {
  static async getAllChildStatus() {
    return await dbClient.query(`select * from child_status`);
  }
}