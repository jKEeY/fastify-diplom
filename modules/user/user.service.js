import { UserModel } from './user.model.js'
import { UserStorage } from './user.storage.js'

export async function getAllUsers() {
  const result = await UserStorage.getAllUsers()
  const users = result.rows.map(user => new UserModel(user))

  return users;
}

export async function getUserById(id) {
  const result = await UserStorage.getUserById(id);
  return new UserModel(result.rows[0]);
}

export async function createUser(userDTO) {
  const result = await UserStorage.createUser(userDTO);

  return new UserModel(result.rows[0]);
}

export async function getUserByLogin(login) {
  const result = await UserStorage.signinUser(login);
  if (!result.rows.length) return null;

  return new UserModel(result.rows[0]);
}
