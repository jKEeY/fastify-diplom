export class CreateUserDTO {
  constructor(userModel) {
    this.email = userModel.email;
    this.login = userModel.login;
    this.role_reference = userModel.role_reference;
    this.password = userModel.password;
  }
}

export class SigninUserDTO {
  constructor(userModel) {
    this.login = userModel.login;
    this.password = userModel.password;
  }
}