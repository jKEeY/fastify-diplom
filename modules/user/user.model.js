export class UserModel {
  constructor(userModel) {
    this.id = userModel.user_id;
    this.email = userModel.email;
    this.login = userModel.login;
    this.role_name = userModel.role_name;
    this.password = userModel.password;
  }
}