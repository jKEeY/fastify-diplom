import { dbClient } from '../../packages/postgresql.js';

export class UserStorage {
  static async getAllUsers() {
    return await dbClient.query({
      rowMode: 'object',
      text: `
        select * from users as t1 
        inner join roles as t2 on t2.role_id = t1.role_reference;
    `});
  }

  static async getUserById(id) {
    return await dbClient.query(`
      select * from users
      where users.id = $1
      inner join roles as t2 on t2.role_id = users.role_reference;
    `, [id])
  }

  static async createUser(user) {
      return await dbClient.query(`
      insert into users email, password, login, role_reference values ($1, $2, $3, $4) returning email, password, login, role_reference, user_id;
    `, [user.email, user.password, user.login, user.role_reference]);
  }

  static async signinUser(login) {
    return await dbClient.query(`
      select user_id, login, password from users
      where users.login = $1;
    `, [login])
  }
}