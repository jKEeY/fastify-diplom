import pg from 'pg';

const { Client } = pg;

class PosgreSQLClient {
  constructor() {
    this.client = new Client({
      host: '127.0.0.1',
      port: 5432,
      database: 'diplom',
      password: '12345',
      user: 'postgres',
    })
  }

  connect() {
    return this.client.connect();
  }
  die() {
    return this.client.end();
  }

  async query(query, values = []) {
    return await this.client.query(query, values);
  }
}

export const dbClient = new PosgreSQLClient()